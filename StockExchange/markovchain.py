import math
import numpy.random as rand
from numpy.linalg import eig
from numpy import argsort, flip
from copy import deepcopy
import scipy.stats as stt

class Markov_Chain:
    def __init__(self,ratemat, states, init_state = None):
        """
        The Markov_Chain class has two main attributes :
        - ratemat : matrix containing the rate of transition from state j to state i over dt, noted r_ij
        - states : a list in which each element is a list (state) which contains :
            - state[0] = generator : the distribution from which the random variable is chosen. Implemented distributions are :
                - constant : returns a constant value. state[1] can be assigned to the desired constant. If not, the default return value is the index of state in states
                - others : other distributions are those implemented in scipy.stats. state[0] will correspond to the name of the function in scipy,stats and
                           additional values in state correspond to mandatory inputs of the corresponding scipy function, in order.
            - state[1] :
                - If state[0] is "constant" : the desired constant
                - Else : additional function parameters, in a list, in order, like described in the corresponding scipy.stats function
        """

        self.ratemat = ratemat
        self.transmat = deepcopy(self.ratemat)
        self.states = states

        for i in range(len(self.ratemat)):
            for j in range(len(self.ratemat)):
                self.transmat[i][j] = self.ratemat[i][j] / sum(self.ratemat[i])
        
        
        self.init_state = rand.randint(0,len(self.states))

        if not (init_state is None):
            self.init_state = init_state

        self.current_state = deepcopy(self.init_state)
        self.state_succession = [deepcopy(self.init_state)]
        self.value_succession = [0]

        if "constant" in self.states[self.init_state]:
            try:
                a = self.states[self.init_state][1]
            except:
                self.value_succession[0] = self.init_state
            else:
                self.value_succession[0] = a
        else:
            self.value_succession[0] = self.states[self.init_state][0].rvs(*self.states[self.init_state][1])

        self.evals, self.evecs = eig(self.transmat)
        self.sorted_evals = flip(argsort(self.evals),0)

    def change_transmat(self,ratemat):
        self.ratemat = ratemat

        for i in range(len(self.ratemat)):
            for j in range(len(self.ratemat)):
                self.transmat[i][j] = self.ratemat[i][j] / sum(self.ratemat[i])

        self.evals, self.evecs = eig(self.transmat)
        self.sorted = flip(argsort(self.evals),0)

    def run_chain(self, n_iters = 1):
        for i in range(n_iters):
            thresh = self.transmat[self.current_state][0]

            next = 0

            floatget = rand.uniform()

            while floatget > thresh:
                next += 1

                thresh += self.transmat[self.current_state][next]

            self.state_succession.append(next)

            self.current_state = next

            if "constant" in self.states[self.current_state]:
                try:
                    a = self.states[self.current_state][1]
                except:
                    self.value_succession.append(self.current_state)
                else:
                    self.value_succession.append(a)
            else:
                self.value_succession.append(self.states[self.init_state][0].rvs(*self.states[self.init_state][1]))

    def reset_chain(self):
        self.init_state = rand.randint(0,len(self.states))
        self.current_state = deepcopy(self.init_state)
        self.state_succession = [deepcopy(self.init_state)]
        self.value_succession = [0]

        if "constant" in self.states[self.init_state]:
            try:
                a = self.states[self.init_state][1]
            except:
                self.value_succession[0] = self.init_state
            else:
                self.value_succession[0] = a
        else:
            self.value_succession[0] = self.states[self.init_state][0].rvs(*self.states[self.init_state][1])