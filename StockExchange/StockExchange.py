import math
import numpy.random as rand
from numpy.linalg import eig
from numpy import argsort, flip
from copy import deepcopy
import scipy.stats as stt
from markovchain import Markov_Chain

from matplotlib import pyplot as plt

# To change : introduce bubble market and crash market

states = [[stt.t, [30,0.00004,0.0004]], [stt.t, [6,0.00004,0.0006]], [stt.t, [3,0.00004,0.0008]], [stt.t, [30,-0.00001,0.0004]]] # States : Stable Market (0), Unstable Market (1), Volatile Market (2), Crash Market (3)

ratemat   = [[100,1,0,3],
             [3,100,1,0],
             [0,3,100,1],
             [1,0,3,100]]

chain = Markov_Chain(ratemat, states, 1)

successions = []

chain_length = 10000

n_plots = 10

for i in range(n_plots):
    chain.reset_chain()

    chain.run_chain(chain_length)

    ret = [1.0]

    for i in range(1,len(chain.value_succession)):
        ret.append(ret[i-1] * math.exp(math.log(10) * chain.value_succession[i]))

    #for i in range(len(ret)):
        #ret[i] = math.log10(ret[i])

    successions.append(ret)

for i in range(n_plots):
    plt.plot(list(range(chain_length + 1)), successions[i])

plt.show()